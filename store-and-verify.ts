const { ethers } = require("hardhat");

// global variable to store constraints extracted from imitator .res file
var extractedConstraints;


/********************************************************************
*********************************************************************
    Functions to match operators
*********************************************************************
********************************************************************/


// function to check if the character is operator or not
function isOperator(x) {
    switch (x) {
        case '+':
        case '-':
        case '*':
        case '/':
        case '>':
        case '<':
        case '=':
        case ']': // to replace >=
        case '[': // to replace <=
            return true;
    }
    return false;
  }
  
  // function to return the string corresponding to the operator
  function getOperator(x) {
    switch (x) {
        case '+':
            return 'add';
        case '-':
            return 'sub';
        case '*':
            return 'mul';
        case '/':
            return 'div';
        case '=':
            return 'eq';
        case '>':
            return 'gt';
        case '<':
            return 'lt';
        case '[': // to replace <=
            return 'le';
        case ']': // to replace >=
            return 'ge';      
    }
    return 'UNDEF';
  }

  // function to find the precedence of an operator
  function precedence(x) {
    switch (x) {
        case '+':
        case '-':
            return 2; // middle
        case '*':
        case '/':
            return 3; // highest
        case '>':
        case '<':
        case '=':
        case ']': // to replace >=
        case '[': // to replace <=
            return 1; // lowest
    }
    return 0;
  }


/********************************************************************
*********************************************************************
    Function to convert infix notation to prefix notation
        input: 2 + 2 * 4 as a String
        output: + 2 * 2 4 as a String
*********************************************************************
********************************************************************/

function infixToPrefix(infix) {
      var i, j;
      var stack = [];
      var prefix = [];
      for (i = infix.length - 1; i >= 0; i--) {
          if (infix[i] == ')') {
              stack.push(infix[i]);
          } else if (infix[i] == '(') {
              while (stack[stack.length - 1] != ')' && stack.length > 0) {
                  var op = stack.pop();
                  prefix.push(op);
              }
              if (stack[stack.length - 1] == ')') {
                  stack.pop();
              }
          } else if (isOperator(infix[i])) {
            // current caracter is an operator
              if (stack.length == 0) {
                  // no operator in the stack 
                  stack.push(infix[i]);
              } else {
                  // operator(s) in the stack
                  while (stack.length > 0) {
                    /* 
                        if the last operator in the stack is of highest precedence than the current operator
                        e.g. * is in the stack, current operator is + 
                        then pop * from the stack and push * to the prefix
                    */   
                      if (precedence(stack[stack.length - 1]) > precedence(infix[i])) {
                        var op = stack.pop();
                        if(prefix[prefix.length - 1] != ' '){
                              prefix.push(' '); // add space between two operators
                        }
                        prefix.push(op);
                       } else {
                          break;
                      }
                  }
                  // push the current operator to the stack 
                  stack.push(infix[i]);
              }
          } else {
              // current caracter is not an operator (e.g. number or parameter)
              if(infix[i] != ' ' || (infix[i] == ' ' && prefix[prefix.length - 1] != ' ')){ // do not push  ' ' if the last caracter of prefix is already ' ' 
                  prefix.push(infix[i]);
              }
          }
      }
      /*
        stack contains only operators
        that occur in the same order 
        as operators and numbers/parameters were pushed in the prefix
        push stack in prefix
      */
      while (stack.length > 0) {
          var op = stack.pop();
          prefix.push(' '); // add space between two operators
          prefix.push(op);
      }
      prefix = prefix.reverse();
      return prefix.join('');
  }
  

// test the infixToPrefix function
// var infix = "1 + 23 * 45";
// var infix = "1 + a1B * 23 = c2 - 4";  // prefix output: = + 1 * a1B 23 - c2 4
// console.log("prefix output: " + infixToPrefix(infix)); // output: + 1 * 23 45


/********************************************************************
*********************************************************************
    Function to convert an expression represented as a String
    e.g. the result of infixToPrefix()
    into an array of numbers/parameters/operators
    and separate the set of parameters from numbers and operators
        input: + b * a 4
        output: [+ b * a 4] and [b a]
*********************************************************************
********************************************************************/

function formatExpression(expression) {
    /* 
        matches: parameters such as 'A1b', 'b1'
                 operators such as +, -, etc. Note that [ stands for <=, ] for >= and needs to be replaced in the input expression
                 numbers
    */
    var expressionArray = expression.match(/[a-zA-Z]+[a-zA-Z0-9_]*|\d+|\+|\*|\/|\-|\=|\<|\>|\[|\]/g);
    //  matches: parameters such as 'A1b', 'b1'
    var onlyParam = expression.match(/[a-zA-Z]+[a-zA-Z0-9_]*/g);
    return [expressionArray, onlyParam];
}

// test the formatExpression function
// var expr = "1 + a1B * 23 = c2 - 4";  
// var [prefixArray, params] = formatExpression(infixToPrefix(expr));
// console.log("expression array output: " + prefixArray.join(' ')); // output : = + 1 * a1B 23 - c2 4
// console.log("param output: " + params.join(' ')); // output : a1B c2


/********************************************************************
*********************************************************************
    Function to create a dictionnary from an array of strings
        input: an array
        output: a dictionnary
*********************************************************************
********************************************************************/
  
function dictionFromArray(array) {
      var i;
      var dictionnary = {};
      for (i = 0; i < array.length; i++) {
        dictionnary[array[i]] = i;
      }
      // print the dictionnary
      for(var key in dictionnary) {
        console.log(key + ' :: ' + dictionnary[key]);
      }
      return dictionnary;
    }
  
    
/********************************************************************
*********************************************************************
    Function to convert an array representing an expression 
    in prefix notation to Z3 infix notation 
        input: = + 1 * a1B 23 - c2 4 as an array e.g. result of formatExpression()
        output: ((1+(a1B*23))=(c2-4)) and 1.add(a1B.mul(23)).eq(c2.sub(4))
*********************************************************************
********************************************************************/

function prefixToInfixZ3(prefix, params, dictName) {
      var stack = []; // infix format easier to read
      var z3Expression = []; // z3 format for javascript's z3-solver
      for (var i = prefix.length - 1; i >= 0; i--) {
        if (isOperator(prefix[i])) {
          // current caracter is an operator
          var currentOperator = prefix[i];
          // LIFO, operand will go left of the operator
          var op1 = stack.pop();
          var leftOp = z3Expression.pop();
          // LIFO, operand will go right of the operator
          var op2 = stack.pop();
          var rightOp = z3Expression.pop();
          // push the expression to the stack
          stack.push("(" + op1 + currentOperator + op2 + ")");
          // push the Z3 expression to z3Expression : op1 is leftOp, op2 is rightOp
          z3Expression.push(leftOp + "." + getOperator(currentOperator) + "(" + rightOp +")");
        } else {
          // current caracter is not an operator (e.g. number or parameter), just push
          stack.push(prefix[i]);
          // if it is a parameter, replace by the Z3 parameter
          if (prefix[i] in params){
            z3Expression.push(dictName + '[\'' + prefix[i] + '\']');
          }
          else { // then it is a number
            z3Expression.push(prefix[i]);
          }
        }
      }
      return [stack.pop(), z3Expression.pop()];  
    }
  
// test the prefixToInfixZ3 function
// [resInf, resZ3] = prefixToInfixZ3(prefixArray);
// console.log("infix output: " + resInf); // output: ((1+(a1B*23))=(c2-4))
// console.log("Z3 output: " + resZ3); // output: 1.add(a1B.mul(23)).eq(c2.sub(4))



    
/********************************************************************
*********************************************************************
    Function to clean imitator format output .res file  
    removes multiline (* comments *) and cleans \n and spaces
    and outputs an array of strings, 
    where each string is a constraint from the .res file
        input: multiline string 
        output: array of strings (constraints)
*********************************************************************
********************************************************************/

function extractConstraintsImitatorResFile(data) {
    // remove multi line comments (* comment *)
    var removeComment = data.replace(/\(\*([\s\S]*?)\*\)/g, '');
    // remove '&' the constraints separator
    var removeAnd = removeComment.replace(/\&/g,'');
    // remove empty spaces at beginning of lines
    var removempty = removeAnd.replace(/^\s*/gm, '');
    // creates an array of strings, each line is a string in the array
    var constraintsArray = removempty.split('\n');
    console.log(`Imitator constraints: array ${constraintsArray.pop()} of size ${constraintsArray.length}`); // must pop to remove the last empty element of the array
    return constraintsArray;
}

/********************************************************************
*********************************************************************
    merge dictionnaries
*********************************************************************
********************************************************************/


function dict_merge(dicts_array){
    // Empty dict for output
    var out_dict = {}
    
    // Loop over array of input dicts
    for(var dict in dicts_array){

        // Loop over keys in dict
        for(var d in dicts_array[dict]){

            // Add / sum dict value depending on presence of key in out_dict
            if(!(d in out_dict)){
                out_dict[d] = dicts_array[dict][d]
            } else {
                //out_dict[d] += dicts_array[dict][d] // or do nothing
            }
        }
    }
    
    return out_dict
}


/********************************************************************
*********************************************************************
    Async Function to create a child_process to call imitator
*********************************************************************
********************************************************************/

// file name
var cheminfichier = "/home/pira/radiateur";
var cheminfichierimi = cheminfichier+".imi";
var cheminfichierres = cheminfichier+".res";

// this function is called in the async main
async function spawnChild() {

const { spawn } = require('node:child_process');
const rmfile = await spawn('rm', [cheminfichierres]); // rm .res file
const imitator = await spawn('imitator', ['-mode','EF',cheminfichierimi,'-output-result']);
const extractConstraints = await spawn('cat', [cheminfichierres]); // extract constraints

rmfile.stdout.on('data', (data) => {
  //console.log(`stdout: ${data}`);
});

rmfile.stderr.on('data', (data) => {
  console.error(`imirmfiletator stderr: ${data}`);
});

rmfile.on('close', (code) => {
  if (code !== 0) {
    console.log(`rmfile process exited with code ${code}`);
  }
}); 


imitator.stdout.on('data', (data) => {
  //console.log(`stdout: ${data}`); // output from imitator
});

imitator.stderr.on('data', (data) => {
  console.error(`imitator stderr: ${data}`);
});

imitator.on('close', (code) => {
  if (code !== 0) {
    console.log(`imitator process exited with code ${code}`);
  }
}); 


extractConstraints.stdout.on('data', (data) => {
    var d = data.toString();
    // stores contraints in the global variable
    extractedConstraints = extractConstraintsImitatorResFile(d);
    //console.log(`stdout extractedConstraints: ${extractedConstraints} of size ${extractedConstraints.length}`);

  });
  
extractConstraints.stderr.on('data', (data) => {
    console.error(`extractConstraints stderr: ${data}`);
  });
  
extractConstraints.on('close', (code) => {
    if (code !== 0) {
      console.log(`extractConstraints process exited with code ${code}`);
    }
  }); 
 
  }


/********************************************************************
*********************************************************************
    Async function Z3: from the global variable extracted constraints
        - creates a Z3 solver
        - transforms the contraints in prefix notation 
          using infixToPrefix
        - get the array of operands, operators and numbers 
          and the array of parameters using formatExpression
        - declares the dictionnary of Z3 variables/constants
          using dictionnaryFromArray
        - transform the constraints in infix Z3 notation 
          using prefixToInfixZ3
        - check sat/unsat and prints a solution if it exists
*********************************************************************
********************************************************************/


const { init } = require('z3-solver');


async function solveConstraintWithZ3(additionalConstraint: string[]) {
    

        const { Context } = await init();
        const { Solver, Int, And, solve, simplify, ast_from_string } = new Context('main');


        /********************************************************************
        *********************************************************************
            Function to create a dictionnary of Z3 consts
              input: an array of strings
              output: a dictionnary of Z3 Int.const
        *********************************************************************
        ********************************************************************/

        function dictionnaryFromArray(array) {
          var i;
          var dictionnary = {};
          for (i = 0; i < array.length; i++) {
            //dictionnary[array[i]] = i;
            dictionnary[array[i]] = Int.const(array[i]);;
          }
          return dictionnary;
        }
      
          const solveMe = new Solver();         
      
          var dictionnaireParam = {};

          // add the additionnal constraints given in parameter
          extractedConstraints.push(...additionalConstraint);
      
          for (var j = 0 ; j< extractedConstraints.length; j++){
              var prefixE = infixToPrefix(extractedConstraints[j].replace(/\<\=/g, '[').replace(/\>\=/g, ']'));
              //console.log("expression : "+prefixE);
              var [prefixArr, parametres] = formatExpression(prefixE);
              var dictionnaireParamtemp = dictionnaryFromArray(parametres);
              //dictionnaireParam = dict_merge([dictionnaireParam, dictionnaireParamtemp]) // merge with function
              dictionnaireParam = {...dictionnaireParam, ...dictionnaireParamtemp}; //spread syntax to merge multiple objects into one new object
              var [resultInfix, resultZ3] = prefixToInfixZ3(prefixArr, dictionnaireParam, "dictionnaireParam");
              //console.log("infix output: " + resultInfix); 
              //console.log("Z3 output: " + resultZ3); 
              var satme = eval(resultZ3);
              //console.log(`equation ${j} : ${satme.toString()}`);
              solveMe.add(And(satme));
          }

          
          // ******************************************************
          // SAT CHECK
          // ******************************************************
          var isSat = await solveMe.check();
          //console.log(isSat); // sat ??

          // ******************************************************
          // in case it is sat, print the model and a solution
          // ******************************************************
          if (isSat.toString().localeCompare("sat") == 0){ // must be 0 if sat
              //console.log(solveMe.toString());
              const myModel = solveMe.model();
              //console.log(myModel.toString());
              //console.log(dictionnaireParam.toString());
              console.log(`\n`+`a solution to the equation: ${extractedConstraints.join(' AND ')} is :`);
              let i = 0;
              for(const p in dictionnaireParam){
                  console.log("8"+"=".repeat(i)+"=> "+`${p} = ${myModel.eval(dictionnaireParam[p])}`);
                  i++;
              }
          }

          return isSat; 

    }


/********************************************************************
*********************************************************************
    Usual main 
*********************************************************************
********************************************************************/


async function main() {

    const [wallet, other1, other2, other3, other4, other5, other6] = await ethers.getSigners();

    // console.log([wallet.address, other1.address, other2.address]);

    //console.log('===> Getting the Factory contract...\n');
    const storage = await ethers.getContractAt('Storage','0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0');

    //console.log('===> Getting values from the storage contract...\n');  
    //console.log("get value at index 3...");
    var additionalConstraintFromContract = ("min_t_r1 = "+(await storage.getAtIndex(3)).toString()).toString();
    console.log("additional constraint with value from contract : ", additionalConstraintFromContract);
    var additionalConstraintFromContract1 = ("max_t_r1 = "+(await storage.getAtIndex(5)).toString()).toString();
    console.log("additional constraint with value from contract : ", additionalConstraintFromContract1);
    

    // ******************************************************
    // calls imitator and extracts constraints
    // ******************************************************
    spawnChild().then( 
        data=> {//console.log("async result of spawnChild:\n" + data);
                },
        err=>  {console.error("async error of spawnChild:\n" + err);}
    );
    
    var toSat = await solveConstraintWithZ3([additionalConstraintFromContract,additionalConstraintFromContract1]) ;

    console.log(toSat);

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });
