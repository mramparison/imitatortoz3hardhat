## Imitator to Z3

Language : Javascript

keywords : imitator, z3-solver, hardhat, smart-contract, raspberry pi


### Prerequities

- Raspberry Pi 4 Model B 4GB RAM aarch64/arm64
- Node >18
- Hardhat 
- [Z3-solver Typescript](https://www.npmjs.com/package/z3-solver)
- [imitator](./imitator) version 2.7.3 (Butter Guéméné) compiled for arm 32 bits (can work on RPi 3 1GB RAM armv7l/armhf)

### Get started

- download and put the ``imitator`` executable in your path environment
- create a new hardhat project
- start the node ``npx hardhat node``

### Hierarchy

- ``Storage.sol`` in the directory ``contracts`` the smart-contract that stores values on the blockchain;
- ``deploy.js`` in the directory ``scripts`` to deploy the smart-contract;
- ``init-storage.ts`` in the directory ``scripts`` to upload new values in the smart-contract;
- ``store-and-verify.ts`` in the directory ``scripts`` to launch ``imitator``, get the constraints and check them against a value stored on the blockchain.


### Structure of ``store-and-verify.ts``

Function ``infixToPrefix()`` takes as input a constraint in infix form and outputs the constraint in prefix form 
```
        input: 2 + 2 * 4 as a String
        output: + 2 * 2 4 as a String
```

Function ``prefixToInfixZ3()`` takes as input a constraint in prefix form and outputs the constraint in infix form for Z3 solver
```
        input: = + 1 * a1B 23 - c2 4 as an array e.g. result of formatExpression()
        output: ((1+(a1B*23))=(c2-4)) and 1.add(a1B.mul(23)).eq(c2.sub(4))
```

Function ``spawnChild()``: create a ``child_process`` to call ``imitator``. Calls the ``extractConstraintsImitatorResFile()`` function and stores the constraints in the global variable ``extractedConstraints``.

Function ``solveConstraintWithZ3()``: from the global variable ``extractedConstraints``
- creates a Z3 solver
- transforms the constraints in prefix notation using infixToPrefix
- get the array of operands, operators and numbers and the array of parameters using formatExpression
- declares the dictionnary of Z3 variables/constants using dictionnaryFromArray
- transform the constraints in infix Z3 notation using prefixToInfixZ3
- check sat/unsat and prints a solution if it exists

Function ``main``: get a value from the smart-contract and creates a new constraint. Calls ``imitator`` through the ``spawnChild()`` function then ``solveConstraintWithZ3`` with the new constraint. Outputs whether ``imitator`` constraints are satisfied in conjunction with the new constraint.

### Resources

- https://microsoft.github.io/z3guide/programming/Z3%20JavaScript%20Examples/
- https://ericpony.github.io/z3py-tutorial/guide-examples.htm
- https://z3prover.github.io/api/html/namespacez3py.html
- https://github.com/Z3Prover/z3/tree/master/src/api/js/examples/high-level

